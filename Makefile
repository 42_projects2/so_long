# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tilman <tilman@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/06/13 19:48:13 by tfriedri          #+#    #+#              #
#    Updated: 2022/08/06 13:49:52 by tilman           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = so_long
BONUS = so_long_bonus
LINUX = so_long_linux
LINUX_BONUS = so_long_linux_bonus
CC = cc
CFLAGS = -Wall -Werror -Wextra
MLXDIR = ./MLX42
MLX = ./MLX42/libmlx42.a
FTPRINTFDIR = ./ft_printf
FTPRINTF= ./ft_printf/libftprintf.a

SRCS =	src/main.c \
		src/map/check_map_1.c \
		src/map/check_map_2.c \
		src/map/check_map_3.c \
		src/map/get_map_1.c \
		src/map/get_map_2.c \
		src/map/get_map_3.c \
		src/other/player_move.c \
		src/other/player_move_axis.c \
		src/other/animations.c \
		src/other/file.c \
		src/other/world_init.c \
		src/other/exit_game.c \
		src/debug.c \

BSRCS = src_bonus/main_bonus.c \
		src_bonus/map/check_map_1_bonus.c \
		src_bonus/map/check_map_2_bonus.c \
		src_bonus/map/check_map_3_bonus.c \
		src_bonus/map/get_map_1_bonus.c \
		src_bonus/map/get_map_2_bonus.c \
		src_bonus/map/get_map_3_bonus.c \
		src_bonus/other/player_move_bonus.c \
		src_bonus/other/player_move_axis_bonus.c \
		src_bonus/other/animations_bonus.c \
		src_bonus/other/file_bonus.c \
		src_bonus/other/world_init_bonus.c \
		src_bonus/other/exit_game_bonus.c \
		src_bonus/other/bonus_bonus.c \
		src_bonus/cop/cop_bonus.c \
		src_bonus/cop/cop_move_bonus.c \

OBJS = $(SRCS:.c=.o)

BOBJS = $(BSRCS:.c=.o)

all: $(NAME)

bonus: $(BONUS)

linux: $(LINUX)

linux_bonus: $(LINUX_BONUS)

$(NAME): $(FTPRINTF) $(MLX) $(OBJS)
	@$(CC) $(CFLAGS) $(OBJS) -L$(FTPRINTFDIR) -lftprintf $(MLX) MLX42/libglfw3.a -framework Cocoa -framework OpenGL -framework IOKit -o $(NAME)

$(BONUS): $(FTPRINTF) $(MLX) $(BOBJS)
	@$(CC) $(CFLAGS) $(BOBJS) -L$(FTPRINTFDIR) -lftprintf $(MLX) MLX42/libglfw3.a -framework Cocoa -framework OpenGL -framework IOKit -o $(BONUS)

$(LINUX): $(FTPRINTF) $(MLX) $(OBJS)
	@$(CC) $(CFLAGS) $(OBJS) -L$(FTPRINTFDIR) -lftprintf $(MLX) -ldl -lglfw -o $(LINUX)

$(LINUX_BONUS): $(FTPRINTF) $(MLX) $(BOBJS)
	@$(CC) $(CFLAGS) $(BOBJS) -L$(FTPRINTFDIR) -lftprintf $(MLX) -ldl -lglfw -o $(LINUX_BONUS)

$(FTPRINTF):
	@make -C $(FTPRINTFDIR)

$(MLX):
	@make -C ./MLX42
	
clean:
	@rm -f $(OBJS) $(BOBJS)
	@cd $(MLXDIR)/ && make clean
	@cd $(FTPRINTFDIR)/ && make clean

fclean: clean
	@rm -f $(NAME)
	@rm -f $(BONUS)
	@rm -f $(LINUX)
	@rm -f $(LINUX_BONUS)
	@rm -f $(MLX)
	@cd $(FTPRINTFDIR)/ && make fclean

re:
ifeq ($(shell test -s so_long && echo yes),yes)
ifeq ($(shell test -s so_long_bonus && echo yes),yes)
	@make fclean
	@make all
	@make bonus
else
	@make fclean
	@make all
endif
else ifeq ($(shell test -s so_long_bonus && echo yes),yes)
		@make fclean
		@make bonus
else
	@make all
endif

norm:
	@norminette ft_printf/ src/ src_bonus/
	
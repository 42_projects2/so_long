/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/07 18:21:53 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 15:57:50 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long_bonus.h"

void	mlx_failed(void)
{
	ft_printf("Error\n%s\n", mlx_strerror(MLX_WINFAIL));
	exit(EXIT_FAILURE);
}

int32_t	start_game(int w_blocks, int h_blocks, char **argv)
{
	mlx_image_t		*game_imgs[16];
	mlx_t			*mlx;
	t_world			*world;

	mlx_set_setting(MLX_STRETCH_IMAGE, 1);
	mlx = mlx_init((64 * w_blocks), (64 * h_blocks), "Bicycle Day", true);
	if (!mlx)
		mlx_failed();
	load_images(mlx, game_imgs);
	load_bonus_images(mlx, game_imgs);
	world = init_world(mlx, game_imgs, w_blocks, h_blocks);
	world->trip_intense = 3;
	get_map(world, argv);
	mlx_close_hook(world->mlx, &press_x_bar, world);
	mlx_loop_hook(world->mlx, &player_move_loop, world);
	mlx_loop_hook(world->mlx, &animation_loop, world);
	mlx_loop_hook(world->mlx, &mandala_loop, world);
	if (world->cop_pos[0] != 0)
		mlx_loop_hook(world->mlx, &cop_move_loop, world);
	mlx_key_hook(world->mlx, &keyhook, world);
	mlx_loop(world->mlx);
	return (EXIT_SUCCESS);
}

int	main(int argc, char **argv)
{
	int	x_blocks;
	int	y_blocks;

	if (argc < 2)
	{
		ft_printf("Error\nneed a map as argument.");
		return (0);
	}
	if (argc > 2)
	{
		ft_printf("Error\ntoo much arguments.");
		return (0);
	}
	if (check_filetype(argv) == 1)
		return (1);
	check_map_1(&x_blocks, &y_blocks, argv);
	start_game(x_blocks, y_blocks, argv);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bonus_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 15:33:36 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:49:28 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	wait_for_exit(void *param)
{
	static int	i = 0;

	i++;
	if (i == 150)
		exit_game(((t_world *)param), MLX_SUCCESS);
}

void	load_bonus_images(mlx_t *mlx, mlx_image_t *game_imgs[])
{
	mlx_texture_t	*game_textures[16];
	int				i;

	i = 11;
	game_textures[12] = mlx_load_png("./sprites/mandala0.png");
	game_textures[13] = mlx_load_png("./sprites/cop.png");
	game_textures[14] = mlx_load_png("./sprites/agressive_cop.png");
	game_textures[15] = mlx_load_png("./sprites/busted.png");
	while (i++ < 15)
	{
		if (game_textures[i] == NULL)
			exit_game(NULL, MLX_INVPNG);
		game_imgs[i] = mlx_texture_to_image(mlx, game_textures[i]);
		mlx_delete_texture(game_textures[i]);
	}
}

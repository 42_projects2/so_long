/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_move_axis_bonus.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/28 17:24:05 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:49:37 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	move_y_axis(void *param, int step_in_pixel, char *png_path)
{
	mlx_texture_t	*player;
	t_world			*world;

	player = mlx_load_png(png_path);
	if (player == NULL)
		exit_game((t_world *)param, MLX_INVPNG);
	world = ((t_world *)param);
	mlx_draw_texture(world->player_img, player, 0, 0);
	mlx_delete_texture(player);
	if (step_in_pixel < 0)
		world->move_dir = 1;
	else
		world->move_dir = 2;
	if (check_player_move(world) == 0)
		return ;
	world->player_img->instances[0].y
		= world->player_img->instances[0].y + step_in_pixel;
	world->move_compl += step_in_pixel;
}

void	move_x_axis(void *param, int step_in_pixel, char *png_path)
{
	mlx_texture_t	*player;
	t_world			*world;

	player = mlx_load_png(png_path);
	if (player == NULL)
		exit_game((t_world *)param, MLX_INVPNG);
	world = ((t_world *)param);
	mlx_draw_texture(world->player_img, player, 0, 0);
	mlx_delete_texture(player);
	if (step_in_pixel < 0)
		world->move_dir = 3;
	else
		world->move_dir = 4;
	if (check_player_move(world) == 0)
		return ;
	world->player_img->instances[0].x
		= world->player_img->instances[0].x + step_in_pixel;
	world->move_compl += step_in_pixel;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_game_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 16:52:26 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:49:32 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	free_map_array(t_world *world)
{
	char			**arr;
	unsigned int	i;

	arr = world->map;
	i = 0;
	while (i < world->w_blocks)
	{
		if (arr[i] != NULL)
			free(arr[i]);
		i++;
	}
	free(world->map);
}

void	free_inst_array(t_world *world)
{
	int				**coll_insta_arr;
	unsigned int	i;
	unsigned int	j;

	coll_insta_arr = world->coll_insta_map;
	i = 0;
	j = 0;
	while (i < world->w_blocks)
	{
		{
			if (coll_insta_arr[i] != NULL)
				free(coll_insta_arr[i]);
			j++;
		}
		i++;
		j = 0;
	}
}

void	press_x_bar(void *param)
{
	exit_game(((t_world *)param), MLX_SUCCESS);
}

void	exit_game(t_world *world, mlx_errno_t err)
{
	if (world != NULL)
	{
		free_map_array(world);
		free_inst_array(world);
		mlx_terminate(world->mlx);
	}
	if (err == 0)
		exit(0);
	ft_printf("Error\n%s\n", mlx_strerror(err));
	exit(1);
}

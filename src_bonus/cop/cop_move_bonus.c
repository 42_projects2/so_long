/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cop_move_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 12:12:19 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:48:49 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	move_cop_x_axis(t_world	*world)
{
	if (world->next_cop_pos[0] > world->cop_pos[0])
		world->cop_img->instances[0].x
			= world->cop_img->instances[0].x + world->cop_speed;
	else if (world->next_cop_pos[0] < world->cop_pos[0])
		world->cop_img->instances[0].x
			= world->cop_img->instances[0].x - world->cop_speed;
}

void	move_cop_y_axis(t_world	*world)
{
	if (world->next_cop_pos[1] > world->cop_pos[1])
		world->cop_img->instances[0].y
			= world->cop_img->instances[0].y + world->cop_speed;
	else if (world->next_cop_pos[1] < world->cop_pos[1])
		world->cop_img->instances[0].y
			= world->cop_img->instances[0].y - world->cop_speed;
}

void	check_cop_x_sight(t_world *world)
{
	int	x_cop;

	x_cop = world->cop_pos[0];
	if (world->cop_pos[1] != world->player_pos[1])
		return ;
	while (x_cop != world->player_pos[0])
	{
		if (world->map[x_cop][world->cop_pos[1]] == '1')
			return ;
		if (x_cop < world->player_pos[0])
			x_cop++;
		if (x_cop > world->player_pos[0])
			x_cop--;
	}
	world->next_cop_run_pos[0] = world->player_pos[0];
	world->next_cop_run_pos[1] = world->player_pos[1];
}

void	check_cop_y_sight(t_world *world)
{
	int	y_cop;

	y_cop = world->cop_pos[1];
	if (world->cop_pos[0] != world->player_pos[0])
		return ;
	while (y_cop != world->player_pos[1])
	{
		if (world->map[world->cop_pos[0]][y_cop] == '1')
			return ;
		if (y_cop < world->player_pos[1])
			y_cop++;
		if (y_cop > world->player_pos[1])
			y_cop--;
	}
	world->next_cop_run_pos[0] = world->player_pos[0];
	world->next_cop_run_pos[1] = world->player_pos[1];
}

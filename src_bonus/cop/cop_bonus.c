/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cop_bonus.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 12:07:49 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:59:13 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	agressive_cop(t_world *world)
{
	mlx_texture_t	*cop;

	cop = mlx_load_png("./sprites/agressive_cop.png");
	if (cop == NULL)
		exit_game(world, MLX_INVPNG);
	mlx_draw_texture(world->cop_img, cop, 0, 0);
	mlx_delete_texture(cop);
	world->next_cop_pos[0] = world->next_cop_run_pos[0];
	world->next_cop_pos[1] = world->next_cop_run_pos[1];
	world->next_cop_run_pos[0] = 0;
	world->next_cop_run_pos[1] = 0;
	world->cop_speed = 4;
}

void	normal_cop(t_world *world)
{
	mlx_texture_t	*cop;

	cop = mlx_load_png("./sprites/cop.png");
	if (cop == NULL)
		exit_game(world, MLX_INVPNG);
	mlx_draw_texture(world->cop_img, cop, 0, 0);
	mlx_delete_texture(cop);
	world->cop_speed = 2;
}

void	get_next_cop_position(t_world *world)
{
	check_cop_x_sight(world);
	check_cop_y_sight(world);
	if (world->next_cop_run_pos[0] != 0 && world->next_cop_run_pos[1] != 0)
	{
		agressive_cop(world);
		return ;
	}
	else
		normal_cop(world);
	if (world->player_pos[0] - world->cop_pos[0] < 0
		&& world->map[world->cop_pos[0] - 1][world->cop_pos[1]] != '1')
		world->next_cop_pos[0] -= 1;
	else if (world->player_pos[0] - world->cop_pos[0] > 0
		&& world->map[world->cop_pos[0] + 1][world->cop_pos[1]] != '1')
		world->next_cop_pos[0] += 1;
	else if (world->player_pos[1] - world->cop_pos[1] < 0
		&& world->map[world->cop_pos[0]][world->cop_pos[1] - 1] != '1')
		world->next_cop_pos[1] -= 1;
	else if (world->player_pos[1] - world->cop_pos[1] > 0
		&& world->map[world->cop_pos[0]][world->cop_pos[1] + 1] != '1')
		world->next_cop_pos[1] += 1;
}

void	busted(t_world *world)
{
	world->pause = 1;
	image_to_window(world, world->busted_img,
		(((world->w_blocks * 64) / 2) - 190),
		(((world->h_blocks * 64) / 2) - 70));
	mlx_loop_hook(world->mlx, &wait_for_exit, world);
}

void	cop_move_loop(void *param)
{
	t_world	*world;

	world = ((t_world *)param);
	if (world->pause == 0)
	{
		if (world->cop_pos[0] == world->next_cop_pos[0]
			&& world->cop_pos[1] == world->next_cop_pos[1])
			get_next_cop_position(world);
		if (world->next_cop_pos[0] != world->cop_pos[0])
			move_cop_x_axis(world);
		if (world->next_cop_pos[1] != world->cop_pos[1])
			move_cop_y_axis(world);
		if (world->cop_img->instances[0].x % 64 == 0)
			world->cop_pos[0] = world->cop_img->instances[0].x / 64;
		if (world->cop_img->instances[0].y % 64 == 0)
			world->cop_pos[1] = world->cop_img->instances[0].y / 64;
		if (world->player_pos[0] == world->cop_pos[0]
			&& world->player_pos[1] == world->cop_pos[1])
			busted(world);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map_3_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/26 14:35:58 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 14:04:38 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	img_to_win_err(t_world *world)
{
	if (world != NULL)
	{
		free_map_array(world);
		free_inst_array(world);
		mlx_terminate(world->mlx);
	}
	ft_printf("Error\nproblem while loading img to window.\n");
	exit(1);
}

int32_t	image_to_window(t_world *world, mlx_image_t *img, int32_t x, int32_t y)
{
	int32_t	ret_val;

	ret_val = mlx_image_to_window(world->mlx, img, x, y);
	if (ret_val == -1)
	{
		img_to_win_err(world);
		return (0);
	}
	else
		return (ret_val);
}

void	draw_exit(t_world *world, unsigned int x, unsigned int y)
{
	if (world->cop_pos[0] == 0)
	{
		image_to_window(world, world->cop_img, x * 64, y * 64);
		world->cop_pos[0] = x;
		world->cop_pos[1] = y;
		world->next_cop_pos[0] = x;
		world->next_cop_pos[1] = y;
	}
	image_to_window(world, world->exit_img, x * 64, y * 64);
}

void	repair_array_walls(t_world *world)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	j = 0;
	while (i < world->w_blocks)
	{
		while (j < world->h_blocks)
		{
			if (world->map[i][j] == '2')
				world->map[i][j] = '1';
			j++;
		}
		i++;
		j = 0;
	}
}

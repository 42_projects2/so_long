/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map_2_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/26 13:07:07 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:59:45 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	unpicture_blocks(t_world *world, int x, int y, mlx_image_t *img)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	j = 0;
	while (i < (img->width / 64))
	{
		while (j < (img->height / 64))
		{
			world->map[i + x][j + y] = '2';
			j++;
		}
		i++;
		j = 0;
	}
}

int	check_wall_block_size(t_world *world, unsigned int x,
	unsigned int y, mlx_image_t *img)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	j = 0;
	if (x + (img->width / 64) > world->w_blocks
		|| y + (img->height / 64) > world->h_blocks)
		return (0);
	while (i < (img->width / 64))
	{
		while (j < (img->height / 64))
		{
			if (world->map[i + x][j + y] != '1' || x + i == 0
				|| y + j == 0 || i + x == world->w_blocks - 1
				|| j + y == world->h_blocks - 1)
				return (0);
			j++;
		}
		i++;
		j = 0;
	}
	return (1);
}

int	check_other_wall_img(t_world *world, mlx_image_t *img,
	unsigned int x, unsigned int y)
{
	if (world->map[x][y] != '1')
		return (1);
	if (check_wall_block_size(world, x, y, img) == 1)
	{
		image_to_window(world, img, x * 64, y * 64);
		unpicture_blocks(world, x, y, img);
		return (1);
	}
	return (0);
}

void	char_is_1(t_world *world, unsigned int x, unsigned int y)
{
	int	img_set;

	img_set = 0;
	if (x == 0 || y == 0 || x == world->w_blocks - 1
		|| y == world->h_blocks - 1)
	{
		image_to_window(world, world->wall_img, x * 64, y * 64);
		img_set++;
	}
	else
	{
		img_set += check_other_wall_img(world, world->roof_3x2_img, x, y);
		img_set += check_other_wall_img(world, world->roof_2x3_img, x, y);
		img_set += check_other_wall_img(world, world->stone_2x2_img, x, y);
		if (x + 6 < world->w_blocks && world->map[x + 6][y] == '0')
			img_set += check_other_wall_img(world, world->truck_6x1_img, x, y);
		if (y + 2 < world->h_blocks && world->map[x][y - 1] != '1'
			&& world->map[x][y + 2] != '1')
			img_set += check_other_wall_img(world, world->car_1x2_img, x, y);
	}
	if (img_set == 0 && (x * y) % 6 != 0)
		image_to_window(world, world->coco_tree_img, x * 64, y * 64);
	else if (img_set == 0)
		image_to_window(world, world->palm_tree_img, x * 64, y * 64);
}

void	array_to_map(t_world *world)
{
	unsigned int	x;
	unsigned int	y;
	char			c;

	x = 0;
	y = 0;
	while (x < world->w_blocks)
	{
		while (y < world->h_blocks)
		{
			c = world->map[x][y];
			if (c == '1')
				char_is_1(world, x, y);
			else if (c == 'C')
				world->coll_insta_map[x][y] = image_to_window(world,
						world->collec_img, x * 64, y * 64);
			else if (c == 'E')
				draw_exit(world, x, y);
			y++;
		}
		y = 0;
		x++;
	}
	repair_array_walls(world);
}

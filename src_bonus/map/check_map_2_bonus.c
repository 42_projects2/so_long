/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_2_bonus.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/26 16:18:07 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 16:23:19 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long_bonus.h"

void	map_err(t_world *world, int fd, char *str)
{
	if (fd != 0)
		close(fd);
	free_map_array(world);
	free_inst_array(world);
	mlx_terminate(world->mlx);
	ft_printf("Error\n%s\n", str);
	exit(1);
}

void	check_map_2(t_world *world)
{
	unsigned int	y;
	unsigned int	x;

	y = 0;
	x = 0;
	while (y < world->h_blocks)
	{
		while (x < world->w_blocks)
		{
			if (y == world->h_blocks - 1 && world->map[x][y] == 0)
				map_err(world, 0, "invalid map: failure in last line.");
			if ((y == 0 || x == 0 || y == world->h_blocks - 1
					|| x == world->w_blocks - 1) && world->map[x][y] != '1')
				map_err(world, 0,
					"invalid map: not rectangular or surrounded by walls.");
			x++;
		}
		x = 0;
		y++;
	}
	y = 0;
	x = 0;
	check_map_3(world);
}

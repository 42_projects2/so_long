/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/07 18:22:56 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:57:19 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H

# include "../MLX42/include/MLX42/MLX42.h"
# include "../ft_printf/ft_printf.h"
# include <stdio.h>
# include <errno.h>
# include <fcntl.h>

typedef struct s_world {
	mlx_t			*mlx;
	mlx_image_t		*empty_img;
	mlx_image_t		*wall_img;
	mlx_image_t		*coco_tree_img;
	mlx_image_t		*palm_tree_img;
	mlx_image_t		*roof_3x2_img;
	mlx_image_t		*roof_2x3_img;
	mlx_image_t		*truck_6x1_img;
	mlx_image_t		*stone_2x2_img;
	mlx_image_t		*car_1x2_img;
	mlx_image_t		*collec_img;
	mlx_image_t		*exit_img;
	mlx_image_t		*player_img;
	unsigned int	w_blocks;
	unsigned int	h_blocks;
	int				move_compl;
	int				move_dir;
	int				moves;
	mlx_image_t		*moves_img;
	char			**map;
	int				**coll_insta_map;
	int				player_pos[2];
	int				new_pp[2];
	mlx_image_t		*collectibles_img;
	int				collectibles;
	int				collected;
	int				pause;
}				t_world;

// main.c
int		main(int argc, char **argv);
// check_map_1.c
void	check_map_1(int *x_blocks, int *y_blocks, char **argv);
// check_map_2.c
void	map_err(t_world *world, int fd, char *str);
void	check_map_2(t_world *world);
// check_map_3.c
void	check_map_3(t_world *world);
// get_map_1.c
int		get_map(t_world *world, char **argv);
// get_map_2.c
void	array_to_map(t_world *world);
// get_map_3.c
void	draw_exit(t_world *world, unsigned int x, unsigned int y);
void	repair_array_walls(t_world *world);
int32_t	image_to_window(t_world *world, mlx_image_t *img, int32_t x, int32_t y);
// world_init.c
t_world	*init_world(mlx_t *mlx, mlx_image_t *game_imgs[], int x, int y);
// player_move.c
int		check_player_move(t_world *world);
void	player_move_loop(void *param);
// player_move_axis.c
void	move_y_axis(void *param, int step_in_pixel, char *png_path);
void	move_x_axis(void *param, int step_in_pixel, char *png_path);
// animations.c
void	animation_loop(void *param);
void	mandala_loop(void *param);
// file.c
int		check_filetype(char **argv);
void	count_moves(t_world *world);
void	count_collectibles(t_world *world, int n_p_s[]);
void	keyhook(mlx_key_data_t keydata, void *param);
void	load_images(mlx_t *mlx, mlx_image_t *game_imgs[]);
// exit_game.c
void	free_map_array(t_world *world);
void	free_inst_array(t_world *world);
void	press_x_bar(void *param);
void	exit_game(t_world *world, mlx_errno_t err);
// debug.c
void	print_map_array(t_world *world);
void	print_inst_map_array(t_world *world);

#endif
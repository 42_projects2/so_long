/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map_1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/07 15:29:02 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:58:27 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

void	character_to_array(char c, t_world *world,
	unsigned int *x, unsigned int *y)
{
	if (c == '0')
		world->map[*x / 64][*y / 64] = '0';
	else if (c == '1')
		world->map[*x / 64][*y / 64] = '1';
	else if (c == 'C')
	{
		world->map[*x / 64][*y / 64] = 'C';
		world->collectibles += 1;
	}
	else if (c == 'E')
		world->map[*x / 64][*y / 64] = 'E';
	else if (c == 'P')
	{
		world->map[*x / 64][*y / 64] = 'P';
		world->player_pos[0] = *x / 64;
		world->player_pos[1] = *y / 64;
	}
	if (c == '\n')
	{
		*y += 64;
		*x = 0;
	}
	else
		*x += 64;
}

void	fill_map_background(t_world *my_world)
{
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	while (y < my_world->h_blocks)
	{
		while (x < my_world->w_blocks)
		{
			image_to_window(my_world,
				my_world->empty_img, x * 64, y * 64);
			x++;
		}
		y++;
		x = 0;
	}
}

void	map_read_loop(t_world *world, int fd)
{
	char			str;
	unsigned int	x;
	unsigned int	y;
	int				read_ret;

	x = 0;
	y = 0;
	read_ret = read(fd, &str, 1);
	while (read_ret > 0)
	{
		if (str != '\n' && x / 64 == world->w_blocks)
			map_err(world, fd, "map not rectangular or surrounded by walls");
		character_to_array(str, world, &x, &y);
		read_ret = read(fd, &str, 1);
	}
	if (read_ret < 0)
	{
		free_map_array(world);
		free_inst_array(world);
		mlx_terminate(world->mlx);
		ft_printf("Error\nproblem while reading the map\n");
		close(fd);
		exit(1);
	}
}

void	player_to_map(t_world *world)
{
	if (world->player_pos[0] != 0)
		image_to_window(world, world->player_img,
			world->player_pos[0] * 64, world->player_pos[1] * 64);
}

int	get_map(t_world *world, char **argv)
{
	int		fd;

	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		exit_game(world, MLX_INVFILE);
	fill_map_background(world);
	map_read_loop(world, fd);
	player_to_map(world);
	check_map_2(world);
	array_to_map(world);
	close(fd);
	return (0);
}

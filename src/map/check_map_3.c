/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_3.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/28 15:43:53 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:46:53 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

void	check_for_start_pos(t_world *world)
{
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	while (x < world->w_blocks)
	{
		while (y < world->h_blocks)
		{
			if (world->map[x][y] == 'P')
				return ;
			y++;
		}
		y = 0;
		x++;
	}
	map_err(world, 0, "map needs a starting position");
}

void	check_for_exit(t_world *world)
{
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	while (x < world->w_blocks)
	{
		while (y < world->h_blocks)
		{
			if (world->map[x][y] == 'E')
				return ;
			y++;
		}
		y = 0;
		x++;
	}
	map_err(world, 0, "map needs an exit");
}

void	check_for_collectibles(t_world *world)
{
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	while (x < world->w_blocks)
	{
		while (y < world->h_blocks)
		{
			if (world->map[x][y] == 'C')
				return ;
			y++;
		}
		y = 0;
		x++;
	}
	map_err(world, 0, "map needs collectibles");
}

void	check_for_spaces(t_world *world)
{
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	while (x < world->w_blocks)
	{
		while (y < world->h_blocks)
		{
			if (world->map[x][y] == '0')
				return ;
			y++;
		}
		y = 0;
		x++;
	}
	map_err(world, 0, "map needs free spaces");
}

void	check_map_3(t_world *world)
{
	check_for_spaces(world);
	check_for_collectibles(world);
	check_for_exit(world);
	check_for_start_pos(world);
}

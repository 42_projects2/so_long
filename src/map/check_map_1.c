/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/07 18:37:58 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 15:47:02 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

int	check_char(char c, int *width, int *x_blocks, int *y_blocks)
{
	if (c != '\n' && c != '0' && c != '1' && c != 'C' && c != 'E' && c != 'P')
	{
		ft_printf("Error\ninvalid map: forbidden char.");
		return (1);
	}
	if (c == '\n')
	{
		if (*y_blocks == 1)
			*width = *x_blocks;
		*x_blocks = 0;
		*y_blocks = *y_blocks + 1;
	}
	else
		*x_blocks = *x_blocks + 1;
	return (0);
}

int	close_fd_and_exit(int fd, char *message, int errno_bool)
{
	close(fd);
	if (errno_bool == 1)
		perror(message);
	else
		ft_printf("%s\n", message);
	exit(EXIT_FAILURE);
}

void	check_map_1(int *x_blocks, int *y_blocks, char **argv)
{
	char	c;
	int		fd;
	ssize_t	r_size;
	int		width;

	*x_blocks = 0;
	*y_blocks = 1;
	width = 0;
	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		close_fd_and_exit(fd, "Error\ncould not open the map\n", 1);
	while (1)
	{
		r_size = read(fd, &c, 1);
		if (r_size < 0)
			close_fd_and_exit(fd, "Error\nproblem while reading the map\n", 1);
		else if (r_size == 0)
			break ;
		else if (check_char(c, &width, x_blocks, y_blocks) != 0)
			close_fd_and_exit(fd, "", 0);
	}
	if (width == 0)
		close_fd_and_exit(fd, "Error\ninvalid map: failure at first line.", 0);
	*x_blocks = width;
	close(fd);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   world_init.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 16:19:05 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:47:14 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

//checked file

char	**calloc_map_array(int x, int y)
{
	char	**arr;
	int		i;

	arr = (char **) ft_calloc(x, sizeof(char *));
	if (arr == NULL)
		exit_game(NULL, MLX_MEMFAIL);
	i = 0;
	while (i < x)
	{
		arr[i] = (char *) ft_calloc(y, sizeof(char));
		if (arr[i] == NULL)
		{
			while (i-- > 0)
				free(arr[i]);
			free(arr);
			exit_game(NULL, MLX_MEMFAIL);
		}
		i++;
	}
	return (arr);
}

int	**calloc_inst_array(int x, int y)
{
	int	**coll_insta_arr;
	int	i;

	coll_insta_arr = (int **) ft_calloc(x, sizeof(int *));
	if (coll_insta_arr == NULL)
		exit_game(NULL, MLX_MEMFAIL);
	i = 0;
	while (i < x)
	{
		coll_insta_arr[i] = (int *) ft_calloc(y, sizeof(int));
		if (coll_insta_arr[i] == NULL)
		{
			while (i-- > 0)
				free(coll_insta_arr[i]);
			free(coll_insta_arr);
			exit_game(NULL, MLX_MEMFAIL);
		}
		i++;
	}
	return (coll_insta_arr);
}

void	init_game_imgs(t_world *world, mlx_image_t *game_imgs[])
{
	world->empty_img = game_imgs[0];
	world->wall_img = game_imgs[1];
	world->coco_tree_img = game_imgs[2];
	world->palm_tree_img = game_imgs[3];
	world->roof_3x2_img = game_imgs[4];
	world->roof_2x3_img = game_imgs[5];
	world->truck_6x1_img = game_imgs[6];
	world->stone_2x2_img = game_imgs[7];
	world->car_1x2_img = game_imgs[8];
	world->collec_img = game_imgs[9];
	world->exit_img = game_imgs[10];
	world->player_img = game_imgs[11];
}

void	free_arrays_and_exit(int x, char **arr, int **coll_insta_arr)
{
	int	i;

	i = x;
	while (i-- > 0)
		free(arr[i]);
	i = x;
	while (i-- > 0)
		free(coll_insta_arr[i]);
	exit_game(NULL, MLX_MEMFAIL);
}

t_world	*init_world(mlx_t *mlx, mlx_image_t *game_imgs[], int x, int y)
{
	t_world	*world;
	char	**arr;
	int		**coll_insta_arr;

	arr = calloc_map_array(x, y);
	coll_insta_arr = calloc_inst_array(x, y);
	world = ft_calloc(1, sizeof(t_world));
	if (world == NULL)
		free_arrays_and_exit(x, arr, coll_insta_arr);
	init_game_imgs(world, game_imgs);
	world->mlx = mlx;
	world->w_blocks = x;
	world->h_blocks = y;
	world->move_compl = 0;
	world->move_dir = 0;
	world->moves = 0;
	world->map = arr;
	world->coll_insta_map = coll_insta_arr;
	world->collectibles = 0;
	world->collected = 0;
	world->pause = 0;
	return (world);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_move.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 12:18:07 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:47:12 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

void	finish(t_world *world)
{
	count_moves(world);
	ft_printf("Finished with %d moves!\n", world->moves);
	exit_game(world, MLX_SUCCESS);
}

void	check_move_complete(t_world *world)
{
	if (world->move_compl == 60 || world->move_compl == -60)
	{
		if (world->map[world->new_pp[0]][world->new_pp[1]] == 'C')
			count_collectibles(world, world->new_pp);
		if (world->map[world->new_pp[0]][world->new_pp[1]] == 'E'
			&& world->collected == world->collectibles)
			finish(world);
		world->map[world->player_pos[0]][world->player_pos[1]] = '0';
		world->player_pos[0] = world->new_pp[0];
		world->player_pos[1] = world->new_pp[1];
		world->map[world->player_pos[0]][world->player_pos[1]] = 'P';
	}
}

int	check_player_move(t_world *world)
{
	world->new_pp[0] = world->player_pos[0];
	world->new_pp[1] = world->player_pos[1];
	if (world->move_dir == 1)
		world->new_pp[1] = world->player_pos[1] - 1;
	else if (world->move_dir == 2)
		world->new_pp[1] = world->player_pos[1] + 1;
	else if (world->move_dir == 3)
		world->new_pp[0] = world->player_pos[0] - 1;
	else if (world->move_dir == 4)
		world->new_pp[0] = world->player_pos[0] + 1;
	if (world->map[world->new_pp[0]][world->new_pp[1]] == '1'
		|| (world->map[world->new_pp[0]][world->new_pp[1]] == 'E'
		&& world->collected != world->collectibles))
		return (0);
	check_move_complete(world);
	return (1);
}

void	player_move_loop(void *param)
{
	int	move_comp;
	int	move_dir;

	if (((t_world *)param)->pause == 0)
	{
	move_comp = ((t_world *)param)->move_compl;
	move_dir = ((t_world *)param)->move_dir;
		if (move_comp != 0 && move_comp % 64 == 0)
		{
			move_comp = 0;
			count_moves(((t_world *)param));
		}
		else if ((mlx_is_key_down(((t_world *)param)->mlx, MLX_KEY_W)
				&& move_comp == 0) || (move_comp != 0 && move_dir == 1))
			move_y_axis(param, -4, "./sprites/bicycle_u.png");
		else if ((mlx_is_key_down(((t_world *)param)->mlx, MLX_KEY_S)
				&& move_comp == 0) || (move_comp != 0 && move_dir == 2))
			move_y_axis(param, 4, "./sprites/bicycle_d.png");
		else if ((mlx_is_key_down(((t_world *)param)->mlx, MLX_KEY_A)
				&& move_comp == 0) || (move_comp != 0 && move_dir == 3))
			move_x_axis(param, -4, "./sprites/bicycle_l.png");
		else if ((mlx_is_key_down(((t_world *)param)->mlx, MLX_KEY_D)
				&& move_comp == 0) || (move_comp != 0 && move_dir == 4))
			move_x_axis(param, 4, "./sprites/bicycle_r.png");
	}
}

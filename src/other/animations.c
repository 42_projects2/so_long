/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   animations.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 12:30:49 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 15:35:38 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

char	*png_path(t_world *world, char *path_begin, int num, char *filetype)
{
	char	*num_str;
	char	*str1;
	char	*str2;

	num_str = ft_itoa(num);
	if (num_str == NULL)
		exit_game(world, MLX_MEMFAIL);
	str1 = ft_strjoin(path_begin, num_str);
	if (str1 == NULL)
	{
		free(num_str);
		exit_game(world, MLX_MEMFAIL);
	}
	str2 = ft_strjoin(str1, filetype);
	if (str2 == NULL)
	{
		free(num_str);
		free(str1);
		exit_game(world, MLX_MEMFAIL);
	}
	free(num_str);
	free(str1);
	return (str2);
}

void	png_to_img(t_world *world, char *png_path, mlx_image_t *image)
{
	mlx_texture_t	*tex;

	tex = mlx_load_png(png_path);
	if (tex == NULL)
		exit_game(world, MLX_INVPNG);
	mlx_draw_texture(image, tex, 0, 0);
	mlx_delete_texture(tex);
	free(png_path);
}

void	animation_loop(void *param)
{
	t_world			*world;
	static int		i = 0;
	static int		j = 0;

	world = ((t_world *)param);
	if (i % 5 == 0)
		png_to_img(world, png_path(world, "./sprites/collectible", i, ".png"),
			world->collec_img);
	if (j % 10 == 0)
		png_to_img(world, png_path(world, "./sprites/exit", j, ".png"),
			world->exit_img);
	i++;
	j += 5;
	if (i == 90)
		i = 0;
	if (j == 360)
		j = 0;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tilman <tilman@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 14:36:37 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/06 13:37:44 by tilman           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

int	check_filetype(char **argv)
{
	char	*p;

	p = ft_strrchr(argv[1], '.');
	if (p)
	{
		if (ft_strncmp(p, ".ber", 4) == 0)
			return (0);
	}
	ft_printf("Error\nmap needs to be a .ber file.");
	return (1);
}

void	count_moves(t_world *world)
{
	world->move_compl = 0;
	world->moves++;
	ft_printf("moves: %d\n", world->moves);
}

void	count_collectibles(t_world *world, int n_p_p[])
{
	world->collected++;
	world->collec_img->instances[world->coll_insta_map
	[n_p_p[0]][n_p_p[1]]].z = 0;
}

void	keyhook(mlx_key_data_t keydata, void *param)
{
	t_world			*world;

	world = ((t_world *)param);
	if (keydata.key == MLX_KEY_ESCAPE && keydata.action == MLX_PRESS)
		exit_game(world, MLX_SUCCESS);
	if (keydata.key == MLX_KEY_M && keydata.action == MLX_PRESS)
		print_map_array(((t_world *)param));
	if (keydata.key == MLX_KEY_I && keydata.action == MLX_PRESS)
		print_inst_map_array(((t_world *)param));
}

void	load_images(mlx_t *mlx, mlx_image_t *game_imgs[])
{
	mlx_texture_t	*game_textures[16];
	int				i;

	i = -1;
	game_textures[0] = mlx_load_png("./sprites/empty.png");
	game_textures[1] = mlx_load_png("./sprites/wall.png");
	game_textures[2] = mlx_load_png("./sprites/coco_tree.png");
	game_textures[3] = mlx_load_png("./sprites/palm_tree.png");
	game_textures[4] = mlx_load_png("./sprites/roof_3x2.png");
	game_textures[5] = mlx_load_png("./sprites/roof_2x3.png");
	game_textures[6] = mlx_load_png("./sprites/truck_6x1.png");
	game_textures[7] = mlx_load_png("./sprites/stone_2x2.png");
	game_textures[8] = mlx_load_png("./sprites/car_1x2.png");
	game_textures[9] = mlx_load_png("./sprites/collectible0.png");
	game_textures[10] = mlx_load_png("./sprites/exit0.png");
	game_textures[11] = mlx_load_png("./sprites/bicycle_r.png");
	while (i++ < 11)
	{
		if (game_textures[i] == NULL)
			exit_game(NULL, MLX_INVPNG);
		game_imgs[i] = mlx_texture_to_image(mlx, game_textures[i]);
		mlx_delete_texture(game_textures[i]);
	}
}

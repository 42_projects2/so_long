/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/19 14:41:13 by tfriedri          #+#    #+#             */
/*   Updated: 2022/08/02 13:47:16 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	print_map_array(t_world *world)
{
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	while (1)
	{
		ft_putchar_fd(world->map[x][y], 1);
		ft_putchar_fd(' ', 1);
		if (x == world->w_blocks - 1 && y == world->h_blocks - 1)
			break ;
		else if (x == world->w_blocks - 1)
		{
			x = 0;
			y++;
			ft_putchar_fd('\n', 1);
		}
		else
			x++;
	}
	ft_putchar_fd('\n', 1);
	ft_putchar_fd('\n', 1);
}

void	print_inst_map_array(t_world *world)
{
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	while (1)
	{
		ft_putnbr_fd(world->coll_insta_map[x][y], 1);
		ft_putchar_fd(' ', 1);
		if (x == world->w_blocks - 1 && y == world->h_blocks - 1)
			break ;
		else if (x == world->w_blocks - 1)
		{
			x = 0;
			y++;
			ft_putchar_fd('\n', 1);
		}
		else
			x++;
	}
	ft_putchar_fd('\n', 1);
	ft_putchar_fd('\n', 1);
}
